// init Isotope
var $grid = $('.colletion-list').isotope({
    // options
  });
  // filter items on button click
  $('.filter-button-group').on( 'click', 'button', function() {
    var filterValue = $(this).attr('data-filter');
    resetFilterButton();
    $(this).addClass('active-filter-btn');
    $grid.isotope({ filter: filterValue });
  });


  var filterBtns = $('.filter-button-group').find('button');
  function resetFilterButton() {
    filterBtns.each(function() {
        $(this).removeClass('active-filter-btn');
    });
  }